package com.varshini.Controller;
import com.varshini.Entity.Item;
import com.varshini.Service.ItemService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
@RestController
@RequestMapping("/item")
@Controller
public class ItemControler
{

    @Autowired
   private  ItemService itemService;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Item>getAllItems()
    {
     return itemService.getAllItems();
    }

   /* @RequestMapping(value="/num",method=RequestMethod.GET)
    public Item getItemByNum(@PathVariable("num") int num)
    {
        return itemService.getItemByNum(num);
    }*/

}
