package com.varshini.Entity;

public class Item {

    private int num;
    private String name;
    private String weight;
    private int price;

    public Item(int num, String name, String weight, int price )
    {
        this.num=num;
        this.name=name;
        this.weight=weight;
        this.price=price;

    }
    public Item() {}

    public  int getNum()
    {
        return num;
    }
    public  void setNum( int num)
    {
        this.num=num;
    }
    public String getName()
    {
        return name;
    }
    public  void setName(String name)
    {
        this.name=name;
    }
    public String getWeight()
    {
        return weight;
    }
    public void  setweight()
    {
        this.weight = weight;
    }

    public int getPrice()
        {
            return price;
        }
        public void setPrice(int price)
         {
        this.price-=price;
         }
    }


